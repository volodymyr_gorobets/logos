<?php

function factorial($a) {
  if($a==1)
    return 1;
  if($a<1)
    return 0;
  $result = 1;
  for($i=2; $i<=$a; $i++) {
    $result=$result*$i;
  }
  return $result;
}

function factorial_rec($a) {
  if($a==1)
    return 1;
  if($a<1)
    return 0;
  $result = $a * factorial_rec($a-1);
  return $result;
}

//echo factorial_rec(5);

$array = [10,23,4,5,7,34];

function simple_sort($a,$b) {
    if(is_simple($a)) {
      if(is_simple($b)) {
        return 0;
      } else return -1;
    } else {
      if(is_simple($b)) {
        return 1;
      } else return 0;
    }
}

function is_simple($a) {
  if($a==0 || $a==1 || $a==2)
    return true;
  
  for($i=2;$i<$a;$i++) {
    if($a % $i == 0)
      return false;
  }
  return true;
}

//uasort($array,"simple_sort");
//print_r($array);

?>