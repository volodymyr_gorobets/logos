<?php
if(isset($_GET["product_id"])) {
  $product = getProductById($_GET["product_id"]);
  require("templates/product.php");
  
}

else if(isset($_GET["category"])) {
  $products = getCategoryProducts($_GET["category"]);
  $category = getCategoryNameById($_GET["category"]);
  require("templates/category.php");
}
?>