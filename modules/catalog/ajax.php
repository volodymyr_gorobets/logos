<?php
session_start();
require("../../core/connect.php");
require("../../core/base_functions.php");
if(isset($_POST["action"])) {
  
  switch($_POST["action"]) {
    case "add":
      $product_id = $_POST["pid"];
      $count = $_POST["count"];
      if(isset($_SESSION["cart"][$product_id]))
        $_SESSION["cart"][$product_id] = $_SESSION["cart"][$product_id] + $count;
      else
        $_SESSION["cart"][$product_id] = $count;
        echo json_encode(array("total" => getCartTotal(true), "message" => "Item was added to cart!"));
    break;
  }
  
}

?>