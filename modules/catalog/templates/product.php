<div class="single">

<div class="container">
<div class="col-md-12">
	<div class="col-md-5 grid">		
		<div class="flexslider">
			  <ul class="slides">
			    <li data-thumb="/assets/images/<?=$product["image"]?>">
			        <div class="thumb-image"> <img src="/assets/images/<?=$product["image"]?>" data-imagezoom="true" class="img-responsive"> </div>
			    </li>
			  </ul>
		</div>
	</div>	
<div class="col-md-7 single-top-in">
        <div class="span_2_of_a1 simpleCart_shelfItem">
				<h3><?=$product["name"]?></h3>
			    <div class="price_single">
				  <span class="reducedfrom item_price">$<?=$product["price"]?></span>
				 <div class="clearfix"></div>
				</div>
				<h4 class="quick">Description:</h4>
				<p class="quick_desc"><?=$product["text"]?></p>
				 <div class="quantity"> 
								<div class="quantity-select">                           
									<div class="entry value-minus">&nbsp;</div>
									<div class="entry value"><span>1</span></div>
									<div class="entry value-plus active">&nbsp;</div>
								</div>
          </div>
							<!--quantity-->
	<script>
    $('.value-plus').on('click', function(){
    	var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)+1;
    	divUpd.text(newVal);
      $(".add-to-cart").attr("product_count",newVal);
    });

    $('.value-minus').on('click', function(){
    	var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10)-1;
    	if(newVal>=1) {
        divUpd.text(newVal);
        $(".add-to-cart").attr("product_count",newVal);
      }
    });
	</script>
	<!--quantity-->
				 
			    <a href="#" class="add-to item_add hvr-skew-backward add-to-cart" product_id="<?=$product["id"]?>" product_count="1">Add to cart</a>
			<div class="clearfix"> </div>
			</div>
		
					</div>
			<div class="clearfix"> </div>
			<!---->
		

  <div class="clearfix"></div>
  </div>
			<!---->	
</div>
<!----->


		<div class="clearfix"> </div>
	</div>
	</div>

<script src="/assets/js/imagezoom.js"></script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script defer src="/assets/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="/assets/css/flexslider.css" type="text/css" media="screen" />

<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});
</script>
