<?php
//отримання і зміна символа в рядку
$str = 'Test string';
$first = $str{6};

$third = $str{2};

$str{1} = 'o';
//echo $str;

//Конкатенація
$a = "Hello ";
$b = $a . "World!";
//$b = $b . $a;
$b.=$a;
//echo $b + $a;

//Довжина рядка 
//strlen(string $str);
$x = "simple string";
//echo strlen($x);

//Пошук рядка в рядку
//strpos(string $where, string $what, int $fromwhere=0)
if(strpos("simple string str","sim") === false) {
 // echo "-";
} else {
 // echo "+";
}



//Частина рядка
//substr(string $str, int $start [,int $length])
$str = "Simple string";
//echo substr($str,0,2); 
//echo substr($str,-3,3);

//Порівняння рядків
//strcmp(string $str1, string $str2)
//strcasecmp(string $str1, string $str2)
//echo strcmp("test","test2");
//echo strcasecmp("Test","test");

//Заміна одного набору символів на інший в рядку
//str_replace(string $from, string $to, string $str)
//echo str_replace("string","","string Simple string");

//Заміна nl  на <br />
//nl2br(string $string)
//echo nl2br("Simple string \n test");

//Видалення тегів
//strip_tags (string $str [, string $allowable_tags])
//echo strip_tags("<p>Simple <b>text</b></p>","<b>");

//Видалення зайвих пробілів
//trim(string $str)
//echo trim(" \n   Simple string  ");


//Заміна символів в рядку
//strtr(string $str, string $from, string $to)
//echo strtr("абв", "абв","abv");

//екранація
//addslashes(string $str)
//stripslashes(string $str)

//Зміна регістра
//strtolower(string $str)
//strtoupper(string $str)

//echo strtolower("Simple string");
//echo strtoupper("Simple string");
//echo ucwords("test string");

//Хеш функції
//md5(string $str) MD5 Message-Digest Algorithm
//echo md5("test");

//crc32(string $str)
//echo crc32("test");

//Буфер виводу
//flush()
?>
