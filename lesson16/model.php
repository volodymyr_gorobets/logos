<?php
require("../core/connect.php");
class Model {
  
  public static $table = "";
  
  private $data;
  
  public static function create($data) {
    $fields = array_keys($data);
    $values = array_values($data);
    foreach($values as $k =>  $value) {
      if(is_string($value)) {
      $values[$k] = "'$value'";
      }
    }
    $q = mysql_query("insert into ".static::$table." (".implode(", ", $fields).") VALUES (".implode(", ",$values).")");
    if($q) {
      $id = mysql_insert_id();
      return static::find($id);
    } else {
      echo mysql_error();
    }
  }
  
  public function __construct($data) {
    $this->data = $data;
  }
  
  public static function find($id) {
    $q = mysql_query("select * from ".static::$table." where id=".$id);
    $res = mysql_fetch_assoc($q);
    return new static($res);
  }
  
  public function __get($param) {
    if(array_key_exists($param, $this->data))
      return $this->data[$param];
    else return NULL;
  }
  
  public function __set($param, $value) {
    if(array_key_exists($param, $this->data))
     $this->data[$param] = $value;
  }
  
  public function save() {
    $data = $this->data;
    $update_string = array();
    foreach($data as $k =>  $value) {
      if(is_string($value)) {
      $value = "'$value'";
      }
      if($k!="id")
        $update_string[] = $k." = ".$value;
    }
    $q = mysql_query("update ".static::$table." set ".implode(",",$update_string)." where id = ".$this->data["id"]);
    if($q)
      return true;
    else 
       echo mysql_error();
  }
  
  public function delete() {
    $q = mysql_query("delete from ".static::$table." where id=".$this->data["id"]);
    $this->data = NULL;
  }
  
}


class Product extends Model {
   public static $table = "products";
}

$data = array(
  "name" => "test product",
  "text" => "test",
  "category_id" => 5,
  "price" => 12,
  "sale" => 0,
  "visible" => 1,
  "image" => "pc2.jpg",
);

//$product = Product::create($data);

$product = Product::find(5);
//echo $product->name;
$product->price = 33;

$product->save();

$product->delete();


