<?php


/**
 * Facebook authentication class - makes requests against Facebook API to authorize user for web site.
 *
 * @author Troy Galloway
 * @version 1.0
 */
class Facebook {

  /**
  * Application ID used by and obtained from Facebook
  *
  * @var string
  */
  const APP_ID = '541053626104814';

  /**
  * Application Secret code used by and obtained from Facebook
  *
  * @var string
  */
  const APP_SECRET = '28e3f0324440ba5fdf9b113840ee6533';

  /**
  * URL used to authenticate user to Facebook
  *
  * @var string
  */
  const URL_DIALOG = 'http://www.facebook.com/dialog/oauth?';

  /**
  * URL used to authenticate server to Facebook
  *
  * @var string
  */
  const URL_AUTH = 'https://graph.facebook.com/oauth/access_token?';

  /**
  * URL used to retrieve information about user from Facebook
  *
  * @var string
  */
  const URL_GRAPH = 'https://graph.facebook.com/me?';

  /**
  * KEY used to identify app id when making url requrest to Facebook
  *
  * @var string
  */
  const KEY_ID = 'client_id';

  /**
  * KEY used to identify where to send the user after authenticating with Facebook
  *
  * @var string
  */
  const KEY_URI = 'redirect_uri';

  /**
  * KEY used for security and to prevent cross-site hacking
  *
  * @var string
  */
  const KEY_STATE = 'state';

  /**
  * KEY used to identify app secret code when making url requrest to Facebook
  *
  * @var string
  */
  const KEY_SECRET = 'client_secret';

  /**
  * KEY used to identify user session to Facebook when connecting server-side
  *
  * @var string
  */
  const KEY_CODE = 'code';

  /**
  * KEY used to identify error in user authentication to Facebook
  *
  * @var string
  */
  const KEY_ERROR = 'error';

  /**
  * KEY used to identify user when retrieving information from Facebook
  *
  * @var string
  */
  const KEY_TOKEN = 'access_token';

  /**
  * URL of where to send user after authentication to Facebook
  *
  * @var string
  * @private
  */
  private $sRedirect = '';

  /**
  * Construct function sets the redirect url
  *
  * @param string $sRedirect
  * @param string $sSessionKey
  * @public
  */
  public function __construct($sRedirect, $sSessionKey = 'UNIQ_ID') {
    $this->sRedirect = $sRedirect;
    $this->sSessionKey = $sSessionKey;
  }

  /**
  * Build the login url for the user and redirect them to Facebook login
  *
  * @param string $sSessionKey
  */
  public function doLogin() {
    $_SESSION[$this->sSessionKey] = md5(uniqid(microtime(true), true));
    $sUrl = sprintf('%s%s=%s&%s=%s&%s=%s', self::URL_DIALOG, self::KEY_ID, self::APP_ID, self::KEY_URI, urlencode($this->sRedirect), self::KEY_STATE, $_SESSION[$this->sSessionKey]);
    header(sprintf('Location: %s', $sUrl));
    exit();
  }

  /**
  * Validate server to Facebook and get user information
  *
  * @return array|boolean
  */
  public function getCredentials() {
    if(checkSet($_GET, self::KEY_ERROR)) {
      return false;
    }
    if(checkVar($_SESSION, $this->sSessionKey) !== checkVar($_GET, self::KEY_STATE)) {
      return false;
    }

    //Auth Server
    $sUrl = sprintf('%s%s=%s&%s=%s&%s=%s&%s=%s', self::URL_AUTH, self::KEY_ID, self::APP_ID, self::KEY_URI, urlencode($this->sRedirect), self::KEY_SECRET, self::APP_SECRET, self::KEY_CODE, checkVar($_GET, self::KEY_CODE));
    $Curl = new Curl($sUrl);
    parse_str($Curl->getData(), $aParams);

    //Get user info
    $sUrl = sprintf('%s%s=%s', self::URL_GRAPH, self::KEY_TOKEN, $aParams['access_token']);
    $Curl = new Curl($sUrl);
    $Data = json_decode($Curl->getData());
    return array('id'=>$Data->id, 'name'=>$Data->name);
  }
}