<?php
/**
 * CURL wrapper class - used to make requests against remote servers
 *
 * @author Troy Galloway
 * @version 1.0
 */
class Curl {

  /**
  * Response data from remote server
  *
  * @var string
  * @private
  */
  private $sData = '';

  /**
  * Sets CURL values and makes CURL request
  *
  * @param string $sUrl
  */
  public function __construct($sUrl) {
    $rCurl = curl_init($sUrl);
    curl_setopt($rCurl, CURLOPT_URL, $sUrl);
    curl_setopt($rCurl, CURLOPT_RETURNTRANSFER, 1);
    $sData = curl_exec($rCurl);
    curl_close($rCurl);
    $this->sData = $sData;
  }

  /**
  * Returns string containing data recieved from remote web site
  *
  * @return string
  */
  public function getData() {
    return $this->sData;
  }
}
/**
 * Check to see if the given key exists within the given array.
 *
 * @param Array $aArray
 * @param String $sKey
 * @return Boolean
 */
function checkSet(Array $aArray, $sKey) {
  if(isset($aArray[$sKey])) {
      return true;
  }
  return false;
}

/**
 * Check to see if the given key exists and return its value.
 *
 * @param Array $aArray
 * @param String $sKey
 * @return Mixed
 */
function checkVar(Array $aArray, $sKey, $doEncode = false) {
  if(checkSet($aArray, $sKey) && (!empty($aArray[$sKey]) || $aArray[$sKey] == '0')) {
    if($doEncode === true) {
      return htmlentities(stripslashes($aArray[$sKey]), ENT_QUOTES);
    }
    return $aArray[$sKey];
  }
  return false;
}