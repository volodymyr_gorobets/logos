<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '541053626104814',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<div class="header">
<div class="container">
		<div class="head">
			<div class=" logo">
				<a href="/"><img src="/assets/images/logo.png" alt=""></a>	
			</div>
		</div>
	</div>
	<div class="header-top">
		<div class="container">
		<div class="col-sm-5 col-md-5  header-login  header-social">
					<ul >
						<? if(isset($_SESSION["user_id"])) {?>
						<li><a href="index.php?page=7">My Profile</a></li>
						<?} else {?>
						<li style="color:#fff;">Register with:</li>
						<li><a href="#" class="facebook-login"><i class="ic1"></i></a></li>
						<?}?>
					</ul>
				</div>
				
			
				<div class="clearfix"> </div>
		</div>
		</div>
		
		<div class="container">
		
			<div class="head-top">
			
		 <div class="col-sm-8 col-md-offset-2 h_menu4">
				<nav class="navbar nav_bottom" role="navigation">
 
 <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header nav_2">
      <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     
   </div> 
   <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
        <ul class="nav navbar-nav nav_1">
						<?php
						$menu = getMainMenu();
						foreach($menu as $item) {
							if(!isset($item["categories"])) {
							?>
							 <li><a class="color" href="index.php?page=<?php echo $item["id"]?>"><?php echo $item["name"]?></a></li>
							<?php } else {
								?>
						<li class="dropdown mega-dropdown active">
			    <a class="color1" href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $item["name"]?><span class="caret"></span></a>				
				<div class="dropdown-menu ">
                    <div class="menu-top">
											<?php 
											foreach($item["categories"] as $category) {
												?>
												<div class="col1">
													<div class="h_nav">
														<h4><?php echo $category["name"]?></h4>
															<ul>
																<?php
																	foreach($category["childs"] as $child) {
																		?>
																		<li><a href="index.php?page=<?php echo $item["id"]?>&category=<?php echo $child["id"]?>"><?php echo $child["name"]?></a></li>
																		<?php
																	}
																?>
															</ul>	
													</div>							
												</div>
											<?php
											}
											?>
						<div class="col1 col5">
						<img src="/assets/images/me.png" class="img-responsive" alt="">
						</div>
						<div class="clearfix"></div>
					</div>                  
				</div>				
			</li>
							<?php
							}
						}
						?>
           
         </ul>
     </div><!-- /.navbar-collapse -->

</nav>
			</div>
			<div class="col-sm-2 search-right"> 
				
					<div class="cart box_1"> 
						<?php
							$total = getCartTotal();
						?>
						<a href="index.php?page=6">
						<h3> 
							<div class="total" style="<? if($total == 0) echo "display:none;";  ?>">
							<span class="simpleCart_total">$<?=number_format($total, 2, ".", " ")?></span></div>
							<img src="/assets/images/cart.png" alt=""/></h3>
						</a>
						<? if($total ==0) {?>
						<p class="empty-cart">Empty Cart</p>
						<?}?>

					</div>
					<div class="clearfix"> </div>
					
						<!----->

						<!---pop-up-box---->					  
			<link href="/assets/css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
			<script src="/assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
			<!---//pop-up-box---->
			<div id="small-dialog" class="mfp-hide">
				<div class="search-top">
					<div class="login-search">
						<input type="submit" value="">
						<input type="text" value="Search.." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search..';}">		
					</div>
					<p>Shopin</p>
				</div>				
			</div>
		 <script>
			$(document).ready(function() {
			$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
			});
																						
			});
		</script>		
						<!----->
			</div>
			<div class="clearfix"></div>
		</div>	
	</div>	
</div>