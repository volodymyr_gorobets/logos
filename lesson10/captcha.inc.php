<?php
Class Captcha {    
    // Дескриптор нашего будущего изображения:   
    var $iCurrientImage = null;   
    // Длина изображения, пиксели:   
    var $iW = 105;   
    // Высота изображения, пиксели:   
    var $iH = 35;   
    // Массив сгенерированных символов:   
    var $iString = array();   
    // Цвет шрифта:   
    var $iFontColor = null;   
    // Путь до файла со шрифтом:   
    var $iFont = '/assets/fonts/Ubuntu-Regular.ttf';   
    
    function GenerateString($l = 5) {   
    // Исходный алфавит:  
	//
    $array_of_chars = array('0','1', '2','3', '4', '5', '6', '7', '8', '9', 'Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M');   
    // Используя цикл, мы на каждом шаге итерации   
    // выбираем случайный элемент массива алфавита   
    // и помещаем все это в массив.   
    for($i = 0; $i < $l; $i ++) {   
        $this -> iString[] = $array_of_chars[mt_rand(0, sizeof($array_of_chars)-1)];   
    }          
} 
function DrawImage() {   
    // Оповещаем барузер о том, что идет gif:   
    header("Content-Type: image/gif");   
    // Непосредственно вывод:   
    imagegif($this -> iCurrientImage);   
    // Ну и уничтожим рисунок для очищения памяти:   
    imagedestroy($this -> iCurrientImage);   
    // Все.  
	} 
function CreateImage() {   
    // Используя дескриптор мы создаем изображение с заданной   
    // длиной и высотой.   
    $this -> iCurrientImage = imagecreatetruecolor($this -> iW, $this -> iH);   
    // Затем создаем белый цвет:   
    $bg_color = imagecolorallocate($this -> iCurrientImage, 185,185,185);   
    // И заливаем им все нашу картинку.   
    imagefill($this -> iCurrientImage, 0, 0, $bg_color);   
}   
function DrawText() {   
    // Создаем цвет для строки:   
    $this -> iFontColor = imagecolorallocate($this -> iCurrientImage, 153, 0, 0);   
    // Задаем начальное положение строки:          
    $x_pos = 5;   
    // Количество сивмолов в строке:   
    $chars = sizeof($this -> iString);   
    // Определяем для каждого символа его возможное размещение:   
    $for_char = round(($this -> iW - $x_pos)/ $chars);   
    // Перебираем каждый символ:   
    foreach($this -> iString as $char) {   
        // Сдвигаем случайно символ от -5 до +5 пикселей.   
        $x_pos_ran = $x_pos + mt_rand(-3,3);   
        // Рисуем символ, случайно меняя его наклон от -45 до 45 градусов   
        // Используем TTF шрифт!   
        imagettftext($this -> iCurrientImage, mt_rand(15, 22), mt_rand(-20, 20), $x_pos_ran, mt_rand(20, 25), $this -> iFontColor, $_SERVER['DOCUMENT_ROOT'].$this -> iFont, $char);   
        // Положение по x следующего символа:    
        $x_pos = $x_pos + $for_char;   
    }          
}   

}	 
?>
