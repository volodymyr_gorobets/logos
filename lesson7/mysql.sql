CREATE TABLE table_name
(
field_name1 type mod
...
field_nameN type mod
[primary key]
[foreign key]
)


INSERT INTO table_name [(fields list)]
VALUES (values list);


UPDATE table_name
SET field1 = value1, ... , fieldN = valueN
[WHERE condition];


DELETE FROM table_name
WHERE condition;


SELECT [DISTINCT|ALL] {*| [field1 AS alias] [,..., fieldN AS alias]}
FROM table_name1 [,..., table_nameN]
[WHERE condition]
[GROUP BY filed list] [HAVING condition]
[ORDER BY field list]


ALTER TABLE table_name
DROP field;


DROP table_name;