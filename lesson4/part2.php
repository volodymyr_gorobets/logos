<?php
//Присвоєння значень масива змінним
//list()
$a = array("one","two");
list($var1,$var2) = $a;

$testphp = [
	"key" => 1,
];

//Створення масиву
//array()
$arr = array(3,5,1,3,62,34);
$arr2 = array(
	"c" => 23,
	"b" => 12,
	"a" => 4
);

//Сортування масивів по зростанню/спаданню значень
//asort()
//arsort()
//arsort($arr);



//Сортування масивів по зростанню/спаданню ключів
//ksort()
//krsort()
//krsort($arr2);
//print_r($arr2);


//Сортування масивів з використанням функції
//uksort()
//uasort()


//Перевертання масивів
//array_reverse()

//print_r(array_reverse($arr));


//Перемішування
// shuffle()
//shuffle($arr);
//print_r($arr);

//Зміна ключів і значень
//array_flip(array $arr)
//array_flip($arr);
//print_r(array_flip($arr));

//Список ключів
//array_keys(array $arr [,mixed $SearchVal])
//print_r(array_keys($arr2));

//Список значень
//array_values(array $arr)
//print_r(array_values($arr2));

//Перевірка на наявність в масиві
//in_array(mixed $val, array $arr)
//var_dump(in_array(3, $arr));

//Кількість співпадінь
//array_count_values(list $List)
//var_dump(array_count_values($arr));

//Зливання масивів
//array_merge($arr1,$arr2)
$A = array(10,11,12);
$B = array(13,14,15,16);
//print_r($A+$B);
//print_r(array_merge($A,$B));

//Частина масива
//array_slice(array $Arr, int $offset [, int $len])
$input = array ("a", "b", "c", "d", "e");
//print_r(array_slice ($input, 2)); 

//Додавання елемента в масив
//array_push(alist &$Arr, mixed $var1 [, mixed $var2, …])
//array_unshift(list &$Arr, mixed $var1 [, mixed $var2, …])
array_push($arr,1000);
//print_r($arr);

//Видалення елемента з масива
//array_pop(list &$Arr)
//array_shift(list &$Arr)
$k = array_pop($arr);
//print_r($arr);

//Унікальні значення масива
//array_unique(array $Arr)
//print_r(array_unique($arr));

//Змінна в масив
$a="Test string";
$b="Some text";
$new_array = compact("a","b");
//print_r($new_array);

//Масив в змінні
//extract(array $Arr [, int $type] [, string $prefix])
extract($new_array);

//Створення діапазону чисел
//range(int $low, int $high)
//print_r(range(12,35));

//Кількість елементів масива
//count($arr)
//echo count($arr);

//Видалення 
//unset
unset($arr[2]);
//print_r($arr);
unset($arr);
//var_dump($arr);

?>
