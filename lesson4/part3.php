<meta charset="utf-8">
<?php
//Цикл з передумовою
/*
while (умова) {
інструкція;
}
*/
/*
$x=0;
while ($x<10) {
$x++;
echo $x;
}
*/
//Цикл з післяумовою
/*
do
{
інструкція;
}
while (умова);
*/
/*
$x = 1;
do {
     echo $x;
} while ($x++<10);
*/
//Цикл з лічильником
/*
for (початкова умова; умова циклу; команда після операції) { інструкція; }
*/
$arr = [1,2,3,6,7];
for ($x=0; $x<count($arr); $x++) {
  //echo $arr[$x];
}
//Цикл перебору
/*
foreach (масив as $ключ=>$значення)
інструкція;
*/

$names = array(3,5,6,2,7);
foreach ($names as $key => $value) {
//echo "<b>$value $key</b><br>";
}

//Переривання циклу
//break;

for ($x=0; $x<10; $x++) {
    if($x>5) break;
   // echo $x;
}

//Пропуск ітерації
//continue

for ($x=0; $x<10; $x++) {
    if($x==5) continue;
    //echo $x;
}

$values = [1,4,6,7,3,2,6,8,0,3,2];
$sum = 0;
foreach($values as $value) {
  if($value > 5) {
    $sum+=$value;
  }
}
//echo "Suma = $sum";

$var1 = 156321456;

$tmp = (string)$var1;

$length = strlen($tmp);
$sum = 0;
for($i = 0; $i<$length; $i++) {
  $sum+=(int)$tmp{$i};
}
//echo "Suma = $sum";

$str = "site.com/catalog/category_1/product2";
//print_r(explode("/",$str));
$array2 = ['test','test2','test3'];
echo "Привет";

?>