<?php

//$arr = array(1,3,4,6);
$arr2 = array(
"key" => 1
);
//Одновимірний масив з числовими індексами
$first[0]="one";
$first[1]="two";
$first[2]="three";
$first[3]="four";
//print_r($first);

//Одновимірний масив з числовими індексами
$second[]="one";
$second[]="two";
$second[]="three";
$second[]="four";
$second[3] = "five";

//Багатовимірний масив з числовими індексами
$arr[0][0]="one";
$arr[0][1]="two";
$arr[0][2]="two two";
$arr[1][0]="three";
$arr[1][1]="four";
$arr[1][2]="five";
$arr[2][0]="six";
$arr[2][1]="seven";
$arr[2][2]="eight";

//print_r($arr);

//Одновимірний асоціативний масив 
$products["one"] = "p1";
$products["two"] = "p2";

//Багатовимірний асоціативний масив 
$products2[0]["name"] = "p1";
$products2[0]["price"] = 12;
$products2[1]["name"] = "p2";
$products2[1]["price"] = 13;
print_r($products2);
?>
