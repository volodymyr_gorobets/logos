$(document).ready(function(){
  
  $(".add-to-cart").click(function(){
      product_id = $(this).attr("product_id");
      count = $(this).attr("product_count");
      $.ajax({
        url: '/modules/catalog/ajax.php',
        method:'POST',
        data: {pid:product_id, count:count, action:"add"},
        async:false,
        success:function(data) {
          data = $.parseJSON(data);
          $(".simpleCart_total").text("$"+data.total);
          $(".total").show();
          $(".empty-cart").hide();
          alert(data.message);
        }
      });
      return false;
  });
  
})