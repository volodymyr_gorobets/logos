<?php

class Product {
  
  public $name;
  
  private $price = 0;
  
  protected $date = "12.12.12";
  
  public function getPrice() {
    return $this->price;
  }
  
  public function setPrice($price) {
    $this->price = $price;
    $this->reCalculate(1);
  }
  
  private function reCalculate($price) {
    echo "Recalculate";
  }
}

$product = new Product();

$product->setPrice(13);
$product->name = "Product1";
echo $product->getPrice();
