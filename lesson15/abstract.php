<?

abstract class Figure {
  
  public $square = 10;
  
  abstract public function printFigure();
  
  public function setSquare($sq) {
    $this->square = $sq;
  }
  
}

interface Draw {
 
  public function Draw();
  
  public function Reset();
}


class Circle extends Figure implements Draw {
  
  public function printFigure() {
    echo "Circle with square - ".$this->square;
  }
}

$c = new Circle();
$c->printFigure();