<?
class Test {
  
  const A = 3;
  
  public $a = 3;
  
  public static function getA() {
    echo self::A;
  }
  
  public function getId() {
    echo $this->a;
  }
  
}

echo Test::A;

$test = new Test();

$test->getId();
