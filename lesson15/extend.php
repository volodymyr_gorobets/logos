<?php

class Figure {
  
  protected $square;
  
  const PI = 3.14;
  
  public function getSquare() {
    echo $this->square;
  }
}

class Rectangle extends Figure {
    
  private $p;
  
  public function setSquare($sq) {
    $this->square = $sq;
  }
  
  public function getSquare() {
    echo "Rectangle square = ";
    parent::getSquare();
  }
}

$rec = new Rectangle();

$rec->setSquare(14);
$rec->getSquare();