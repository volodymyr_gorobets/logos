<?php

class News {
  
  public $title;
  
  public $author;
  
  private $data;
  
  public function __construct($title, $author) {
      $this->title = $title;
      $this->author = $author;
    echo "Object created<br/>";
  }
  
  public function printNews() {
    echo "News";
  }
  
  public function __destruct() {
   // echo "Object deleted"; 
  }
  
  public function __get($property) {
      echo $property." - ".$this->data[$property];
  }
  
  public function __set($key,$value) {
    $this->data[$key]  = $value;
  }
  
  public function __call($method, $args) {
    echo $method;
  }
  
  public function __clone() {
    echo "Object cloned";
  }
  
}

$pub = new News("First news", "Admin");
//var_dump($pub);
//unset($pub);
$pub->description = "text text";

echo $pub->description;

$b = clone $pub;