<?

trait Debug {
  public function debug_info() {
    echo "<pre>";
    var_dump($this);
    echo "</pre>";
  }
}

class Test {
  
  use Debug;
  
  public $a;
  
  public $b;
  
  public function __construct($a, $b) {
    $this->a = $a;
    $this->b = $b;
  }
  
}

$test = new Test(1,5);
$test->debug_info();