-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Вер 24 2016 р., 10:01
-- Версія сервера: 5.5.38-0ubuntu0.14.04.1
-- Версія PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `myshop`
--

-- --------------------------------------------------------

--
-- Структура таблиці `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary index',
  `name` char(200) NOT NULL,
  `text` text NOT NULL,
  `parent_id` int(11) NOT NULL,
  `visible` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='our categories table' AUTO_INCREMENT=7 ;

--
-- Дамп даних таблиці `categories`
--

INSERT INTO `categories` (`id`, `name`, `text`, `parent_id`, `visible`, `created_at`) VALUES
(1, 'Телефони', 'Телефони', 0, 1, '2016-09-10 12:12:05'),
(4, 'Nokia', 'test text', 1, 1, '2016-09-10 13:18:15'),
(5, 'Samsung', '', 1, 1, '2016-09-11 13:31:45'),
(6, 'Xiaomi', '', 1, 1, '2016-09-11 13:31:45');

-- --------------------------------------------------------

--
-- Структура таблиці `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `module` char(40) NOT NULL,
  `menu` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп даних таблиці `pages`
--

INSERT INTO `pages` (`id`, `name`, `text`, `module`, `menu`) VALUES
(1, 'Home', '', 'home', 1),
(2, 'Catalog', '', 'catalog', 1),
(3, 'Sale', '', '', 1),
(4, 'About Us', '', '', 1),
(5, 'Contact Us', '', '', 1),
(6, 'Cart', '', 'cart', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(200) NOT NULL,
  `text` text NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `price` float NOT NULL,
  `sale` int(11) NOT NULL DEFAULT '0',
  `visible` enum('0','1') NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `products`
--

INSERT INTO `products` (`id`, `name`, `text`, `category_id`, `price`, `sale`, `visible`, `image`, `created_at`) VALUES
(1, 'Samsung Galaxy S6', 'Samsung Galaxy S6 Samsung Galaxy S6 Samsung Galaxy S6 Samsung Galaxy S6 Samsung Galaxy S6Samsung Galaxy S6 Samsung Galaxy S6 Samsung Galaxy S6 Samsung Galaxy S6 Samsung Galaxy S6 Samsung Galaxy S6 Samsung Galaxy S6', 5, 12000, 10, '1', 'noimage.jpg', '2016-09-10 12:13:43'),
(2, 'Samsung Galaxy S7', 'Samsung Galaxy S7', 5, 100, 0, '1', 'pc2.jpg', '2016-09-10 12:13:43'),
(3, 'Samsung Galaxy S72', 'Samsung Galaxy S72', 5, 100, 0, '1', 'pc4.jpg', '2016-09-10 12:13:43');

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_CATEGORY` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
